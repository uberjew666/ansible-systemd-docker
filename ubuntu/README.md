# Ubuntu images

You can use this image as a base container to run systemd services inside.

## Supported tags

- `latest`, `24.10`
- `24.04`
- `22.04`
- `20.04`

## Usage
