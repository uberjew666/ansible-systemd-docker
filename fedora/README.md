# Fedora images

You can use this image as a base container to run systemd services inside.

## Supported tags

- `latest`, `41`
- `40`

## Usage
