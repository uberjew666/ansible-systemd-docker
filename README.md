# Molecule Systemd Images

Base container images for testing using Ansible Molecule test framework

## About Ansible Molecule

Molecule project is designed to aid in the development and testing of Ansible roles.

Molecule provides support for testing with multiple instances, operating systems and distributions, virtualization providers, test frameworks and testing scenarios.

Molecule encourages an approach that results in consistently developed roles that are well-written, easily understood and maintained.

## Supported Images

* [Amazon Linux](registry.gitlab.com/uberjew666/ansible-systemd-docker:amazonlinux)
  * `latest`, `2`

* [Debian](registry.gitlab.com/uberjew666/ansible-systemd-docker:debian)
  * `latest`, `12`
  * `11`

* [Fedora](registry.gitlab.com/uberjew666/ansible-systemd-docker:fedora)
  * `latest`, `41`
  * `40`

* [Ubuntu](registry.gitlab.com/uberjew666/ansible-systemd-docker:ubuntu)
  * `latest`, `24.10`
  * `24.04`
  * `22.04`
  * `20.04`

## How to Use

To use these images edit the `platforms` section of file `molecule/default/molecule.yml` as follows.

``` yml
...

platforms:
- name: amazonlinux-2
  hostname: amazonlinux
  image: registry.gitlab.com/uberjew666/ansible-systemd-docker:amazonlinux-2
  volumes:
    - /sys/fs/cgroup:/sys/fs/cgroup:ro
  privileged: true
  pre_build_image: true
  override_command: false

- name: centos-8
  hostname: centos
  image: registry.gitlab.com/uberjew666/ansible-systemd-docker:centos-8
  volumes:
    - /sys/fs/cgroup:/sys/fs/cgroup:ro
  privileged: true
  pre_build_image: true
  override_command: false

- name: debian-12
  hostname: debian
  image: registry.gitlab.com/uberjew666/ansible-systemd-docker:debian-12
  volumes:
    - /sys/fs/cgroup:/sys/fs/cgroup:ro
  privileged: true
  pre_build_image: true
  override_command: false

- name: fedora-41
  hostname: fedora
  image: registry.gitlab.com/uberjew666/ansible-systemd-docker:fedora-41
  volumes:
    - /sys/fs/cgroup:/sys/fs/cgroup:ro
  privileged: true
  pre_build_image: true
  override_command: false

- name: ubuntu-24
  hostname: ubuntu
  image: registry.gitlab.com/uberjew666/ansible-systemd-docker:ubuntu-24.04
  volumes:
    - /sys/fs/cgroup:/sys/fs/cgroup:ro
  privileged: true
  pre_build_image: true
  override_command: false

...
```

The default Molecule Docker driver executes Ansible playbooks as the `root` user. If your workflow requires a non-privileged user, edit the `provisioner` section of file `molecule/default/molecule.yml` as follows.

``` yml
...
provisioner:
  name: ansible
  config_options:
    defaults:
      remote_user: ansible
...
```

## Important Note

I use this image for testing roles and playbooks using Ansible Molecule running locally inside in an isolated environment — not for production — and the settings and configuration used may not be suitable for a secure and performant production environment.
